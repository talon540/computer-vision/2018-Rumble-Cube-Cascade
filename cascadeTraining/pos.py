import cv2
import numpy as np
import sys

imgPath = sys.argv[1]
img = cv2.imread(imgPath)
img = cv2.resize(img, (75, 75))
cv2.imwrite(str(imgPath), img)